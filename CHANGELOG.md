### 1.3.0

* Drop support for ruby 2.6 since it has reached EOL

### 1.2.0

* Drop support for ruby 2.5
* Fix activemodel deprecation warnings

### 1.1.1

* Fix json serialization for agent_info

### 1.1.0

* Default Config.api_token to ENV['INFORMANT_API_KEY']
* Remove Config.client_identifier
