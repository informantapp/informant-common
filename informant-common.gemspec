lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift lib unless $LOAD_PATH.include?(lib)

require 'informant-common/version'

Gem::Specification.new do |s|
  s.name = 'informant-common'
  s.version = InformantCommon::VERSION
  s.license = 'MIT'

  s.authors = ['Informant, LLC']
  s.email = ['support@informantapp.com']
  s.description = 'This gem contains the common components for reporting data to the Informant.'
  s.homepage = 'https://www.informantapp.com'
  s.require_paths = ['lib']
  s.summary = 'The Informant tracks server-side validation errors and gives you metrics you never dreamed of.'

  s.required_ruby_version = '>= 2.7.0'
  s.add_development_dependency('appraisal')

  s.files = Dir.glob('lib/**/*') + %w[LICENSE README.markdown Rakefile]
  s.require_path = 'lib'

  s.metadata = {
    'rubygems_mfa_required' => 'true'
  }
end
