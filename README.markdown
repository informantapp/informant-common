## Informant Common

The informant-common gem provides underlying common functionality for building
a framework-specific client in Ruby.

Please see [informant-rails](https://gitlab.com/informantapp/informant-rails) for the official Ruby on Rails client.

[![Homepage](https://s3.amazonaws.com/assets.heroku.com/addons.heroku.com/icons/1347/original.png)](https://www.informantapp.com)

## Compatibility

The Informant officially supports Ruby 2.5+.

[![pipeline status](https://gitlab.com/informantapp/informant-common/badges/master/pipeline.svg)](https://gitlab.com/informantapp/informant-common/-/commits/master)
