require 'bundler/setup'
require 'webmock/rspec'

Bundler.require

class TestSuiteCapabilities
  def self.active_model?
    @active_model ||= defined?(ActiveModel)
  end

  def self.enabled?(capability)
    return true if capability.nil?

    public_send("#{capability}?")
  end
end

ENV['INFORMANT_API_KEY'] = 'abc123'
require 'informant-common'

Dir[File.expand_path(File.join(File.dirname(__FILE__), 'support', '**', '*.rb'))].sort.each { |f| require f }

RSpec.configure do |config|
  config.before do
    InformantCommon::Config.reset!
    InformantCommon::ParameterFilter.reset!
    InformantCommon::Client.reset_transaction!
    InformantCommon::Client.enable!
  end

  config.around do |example|
    example.run if TestSuiteCapabilities.enabled?(example.metadata[:depends_on])
  end
end
