require 'spec_helper'

describe InformantCommon::ParameterFilter do
  describe '.filter' do
    subject { described_class.filter(name, value) }

    let(:name) { 'my_field' }
    let(:value) { 'my_value' }

    before { described_class.instance_variable_set(:@matcher, nil) }

    context 'with value tracking disabled' do
      before { InformantCommon::Config.value_tracking = false }

      context 'with a value present' do
        it { is_expected.to eq('[FILTERED]') }
      end

      context 'with no value present' do
        let(:value) { nil }

        it { is_expected.to eq('[FILTERED]') }
      end
    end

    context 'with value tracking enabled' do
      context 'with a matching field level filter' do
        before { InformantCommon::Config.filter_parameters << 'my_field' }

        it { is_expected.to eq('[FILTERED]') }
      end

      context 'with no matching field level filter' do
        before { InformantCommon::Config.filter_parameters << 'other_field' }

        it { is_expected.to eq(value) }
      end

      context 'with no field level filters at all' do
        it { is_expected.to eq(value) }
      end

      context 'with no value present' do
        let(:value) { nil }

        it { is_expected.to eq(value) }
      end
    end
  end
end
