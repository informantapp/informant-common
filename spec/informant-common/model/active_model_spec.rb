require 'spec_helper'

describe InformantCommon::Model::ActiveModel, depends_on: :active_model do
  describe '#initialize' do
    let(:test_model) { TestModel.new.tap(&:valid?) }
    let(:model) { described_class.new(test_model) }

    it 'stores the passed-in data' do
      expect(model.id).to eq(test_model.object_id)
      expect(model.name).to eq('TestModel')
      expect(model.errors.length).to eq(1)
      expect(model.errors.first.name).to eq('name')
      expect(model.errors.first.value).to be_nil
      expect(model.errors.first.message).to eq('is too short (minimum is 3 characters)')
    end
  end
end
