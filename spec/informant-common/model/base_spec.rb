require 'spec_helper'

describe InformantCommon::Model::Base do
  describe '#initialize' do
    let(:model) { described_class.new(id: 'abc123', name: 'model name', field_errors: %i[a b]) }

    it 'stores the passed-in data' do
      expect(model.id).to eq('abc123')
      expect(model.name).to eq('model name')
      expect(model.errors).to eq(%i[a b])
    end
  end

  describe '#add_error' do
    let(:model) { described_class.new }

    before do
      model.add_error('field', 'value', 'message')
    end

    it 'stores the error' do
      expect(model.errors.length).to eq(1)

      error = model.errors.first
      expect(error.name).to eq('field')
      expect(error.value).to eq('value')
      expect(error.message).to eq('message')
    end
  end

  describe '#to_json' do
    let(:model) { described_class.new(id: 1, name: 'User') }

    before do
      model.add_error('email', nil, "can't be blank")
      model.add_error('name', 'a', 'is too short (minimum is 2 characters)')
    end

    it 'correctly serializes' do
      expect(model.to_json).to eq({
        name: 'User',
        errors: [
          { name: 'email', value: nil, message: "can't be blank" },
          { name: 'name', value: 'a', message: 'is too short (minimum is 2 characters)' }
        ]
      }.to_json)
    end
  end
end
