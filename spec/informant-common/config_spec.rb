require 'spec_helper'

describe InformantCommon::Config do
  describe 'defaults' do
    it 'sets them reasonably' do
      expect(described_class.api_token).to eq(ENV.fetch('INFORMANT_API_KEY', nil))
      expect(described_class.enabled?).to be true
      expect(described_class.exclude_models).to eq([])
      expect(described_class.filter_parameters).to eq([])
      expect(described_class.value_tracking?).to be true
      expect(described_class.collector_host).to eq('https://collector-api.informantapp.com')
    end
  end

  describe '#configure' do
    before do
      described_class.configure do |c|
        c.api_token = 'def456'
        c.exclude_models << :test_model
        c.filter_parameters << :some_param
        c.value_tracking = false
      end
    end

    it 'saves the settings' do
      expect(described_class.api_token).to eq('def456')
      expect(described_class.exclude_models).to eq(%i[test_model])
      expect(described_class.filter_parameters).to eq(%i[some_param])
      expect(described_class.value_tracking?).to be(false)
    end
  end
end
