require 'spec_helper'

describe InformantCommon::Client do
  let(:request_method) { 'POST' }

  describe '.start_transaction!' do
    let(:current_transaction) { described_class.current_transaction }

    context 'when enabled' do
      before { described_class.start_transaction!(request_method) }

      context 'with POST requests' do
        let(:request_method) { 'POST' }

        it 'creates a wrapper' do
          expect(current_transaction).to be_instance_of(InformantCommon::Event::FormSubmission)
        end
      end

      context 'with PATCH requests' do
        let(:request_method) { 'PATCH' }

        it 'creates a wrapper' do
          expect(current_transaction).to be_instance_of(InformantCommon::Event::FormSubmission)
        end
      end

      context 'with PUT requests' do
        let(:request_method) { 'PUT' }

        it 'creates a wrapper' do
          expect(current_transaction).to be_instance_of(InformantCommon::Event::FormSubmission)
        end
      end

      context 'with DELETE requests' do
        let(:request_method) { 'DELETE' }

        it 'creates a wrapper' do
          expect(current_transaction).to be_instance_of(InformantCommon::Event::FormSubmission)
        end
      end

      context 'with OPTIONS requests' do
        let(:request_method) { 'OPTIONS' }

        it 'creates a mock wrapper' do
          expect(current_transaction).to be_an_instance_of(InformantCommon::Event::MockFormSubmission)
        end
      end

      context 'with HEAD requests' do
        let(:request_method) { 'HEAD' }

        it 'creates a mock wrapper' do
          expect(current_transaction).to be_an_instance_of(InformantCommon::Event::MockFormSubmission)
        end
      end

      context 'with GET requests' do
        let(:request_method) { 'GET' }

        it 'creates a mock wrapper' do
          expect(current_transaction).to be_an_instance_of(InformantCommon::Event::MockFormSubmission)
        end
      end
    end

    context 'when disabled' do
      let(:request_method) { 'POST' }

      before do
        described_class.disable!
        described_class.start_transaction!(request_method)
      end

      it 'creates a mock wrapper' do
        expect(current_transaction).to be_an_instance_of(InformantCommon::Event::MockFormSubmission)
      end
    end
  end

  describe '.transmit' do
    let(:model) do
      InformantCommon::Model::Base.new(name: 'User').tap do |m|
        m.add_error('email', nil, "can't be blank")
      end
    end
    let!(:request_stub) do
      stub_request(:post, 'https://collector-api.informantapp.com/v2/form-submissions')
        .with(
          body: {
            name: nil,
            models: [{
              name: 'User',
              errors: [{
                name: 'email',
                value: nil,
                message: "can't be blank"
              }]
            }]
          }
        )
    end

    it 'sends the data to the informant' do
      described_class.start_transaction!(request_method)
      described_class.current_transaction.process_model(model)
      described_class.transmit(described_class.current_transaction).join
      expect(request_stub).to have_been_requested
    end
  end

  describe '.process' do
    let(:model) do
      InformantCommon::Model::Base.new(name: 'User').tap do |m|
        m.add_error('email', nil, "can't be blank")
      end
    end
    let!(:request_stub) do
      stub_request(:post, 'https://collector-api.informantapp.com/v2/form-submissions')
        .with(
          body: {
            name: nil,
            models: [{
              name: 'User',
              errors: [{
                name: 'email',
                value: nil,
                message: "can't be blank"
              }]
            }]
          }
        )
    end

    before do
      described_class.start_transaction!(request_method)
      described_class.current_transaction.process_model(model)
    end

    it 'sends errors to informant and clears the cache' do
      described_class.process.join

      expect(Thread.current[:informant_transaction]).to be_nil
    end

    context 'when receiving a 401' do
      before do
        request_stub.to_return(status: 401)
        described_class.process.join
      end

      it 'disables further tracking' do
        expect(described_class.enabled?).to be(false)
      end
    end
  end
end
