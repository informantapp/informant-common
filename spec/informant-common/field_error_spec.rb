require 'spec_helper'

describe InformantCommon::FieldError do
  describe '#value' do
    subject { field_error.value }

    let(:field_error) { described_class.new(field_name, 'my value') }

    before { InformantCommon::Config.filter_parameters = [:password] }

    context 'when a filtered attribute' do
      context 'with a partial match' do
        let(:field_name) { 'password_confirmation' }

        it { is_expected.to eq('[FILTERED]') }
      end

      context 'with an exact match' do
        let(:field_name) { 'password' }

        it { is_expected.to eq('[FILTERED]') }
      end
    end

    context 'when an unfiltered attribute' do
      let(:field_name) { 'name' }

      it { is_expected.to eq('my value') }
    end
  end
end
