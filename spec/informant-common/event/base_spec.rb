require 'spec_helper'

describe InformantCommon::Event::Base do
  let(:request) { described_class.new }

  describe '#post_request' do
    let(:net_http_post) { double }

    before do
      InformantCommon::Config.api_token = 'abc123'
      allow(described_class).to receive(:endpoint).and_return('http://example.com/test')
    end

    it 'prepares the post request' do
      allow(Net::HTTP::Post).to receive(:new) { net_http_post }
      allow(net_http_post).to receive(:body=)

      expect(request.post_request).to eq(net_http_post)
      expect(net_http_post).to have_received(:body=).with(request.to_json)
      expect(Net::HTTP::Post).to have_received(:new).with(request.endpoint,
                                                          'Authorization' => 'Token token="abc123"',
                                                          'Content-Type' => 'application/json')
    end
  end
end
