require 'spec_helper'

describe InformantCommon::Event::FormSubmission do
  let(:request) { described_class.new }

  describe '#process_model' do
    context 'with an included model' do
      let(:model) { InformantCommon::Model::Base.new(id: 1, name: 'User') }

      before do
        request.process_model(model)
      end

      it 'stores the model' do
        expect(request.models).to eq([model])
      end
    end

    context 'with an excluded model' do
      let(:model) { InformantCommon::Model::Base.new(id: 1, name: 'User') }

      before do
        InformantCommon::Config.exclude_models << 'User'
        request.process_model(model)
      end

      it 'does not store the model' do
        expect(request.models).to eq([])
      end
    end

    context 'without a model' do
      before { request.process_model(nil) }

      it 'does not add the model to the cache' do
        expect(request.models).to be_empty
      end
    end

    context 'with a model that has already been stored' do
      let(:first_model) { InformantCommon::Model::Base.new(id: 1) }
      let(:second_model) { InformantCommon::Model::Base.new(id: 1) }

      before do
        request.process_model(first_model)
        request.process_model(second_model)
      end

      it 'only stores the lastest model' do
        expect(request.models).to eq([second_model])
      end
    end
  end

  describe '#as_json' do
    let(:model) { InformantCommon::Model::Base.new(id: 1, name: 'User') }
    let(:request_json) { request.as_json }

    before do
      model.add_error('email', nil, "can't be blank")
      model.add_error('name', 'a', 'is too short (minimum is 2 characters)')
    end

    context 'with all the data' do
      before do
        request.handler = 'UsersController#create'
        request.process_model(model)
      end

      it do
        expect(request_json).to eq({
                                     name: 'UsersController#create',
                                     models: [{
                                       name: 'User',
                                       errors: [
                                         { name: 'email', value: nil, message: "can't be blank" },
                                         { name: 'name', value: 'a', message: 'is too short (minimum is 2 characters)' }
                                       ]
                                     }]
                                   })
      end
    end

    context 'without endpoint any data' do
      it do
        expect(request_json).to eq({
                                     name: nil,
                                     models: []
                                   })
      end
    end
  end

  describe '#to_json' do
    it 'serializes to json' do
      expect(described_class.new.to_json).to eq({
        name: nil,
        models: []
      }.to_json)
    end
  end

  describe '.endpoint' do
    it 'points to the v2 collector api endpoint' do
      expect(described_class.endpoint).to eq(URI("#{InformantCommon::Config.collector_host}/v2/form-submissions"))
    end
  end

  describe '#valid?' do
    let(:form_submission) { described_class.new }

    context 'with models' do
      before do
        form_submission.models << ''
      end

      it 'is true' do
        expect(form_submission.valid?).to be(true)
      end
    end

    context 'without models' do
      it 'is false' do
        expect(form_submission.valid?).to be(false)
      end
    end
  end
end
