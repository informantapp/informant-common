require 'spec_helper'

describe InformantCommon::Event::AgentInfo do
  describe '#as_json' do
    context 'without overrides' do
      it 'includes the default agent data' do
        expect(described_class.new.as_json).to eq(
          agent_identifier: nil,
          framework_version: nil,
          runtime_version: "ruby-#{RUBY_VERSION}",
          mongoid_version: defined?(Mongoid) ? Mongoid::VERSION : nil
        )
      end
    end

    context 'with overrides' do
      let(:agent_info) do
        described_class.new(
          agent_identifier: 'agent-test',
          framework_version: 'framework-test'
        )
      end

      it 'includes the provided agent data' do
        expect(agent_info.as_json).to eq(
          agent_identifier: 'agent-test',
          framework_version: 'framework-test',
          runtime_version: "ruby-#{RUBY_VERSION}",
          mongoid_version: defined?(Mongoid) ? Mongoid::VERSION : nil
        )
      end
    end
  end

  describe '#to_json' do
    it 'serializes to json' do
      expect(described_class.new.to_json).to eq({
        agent_identifier: nil,
        framework_version: nil,
        runtime_version: "ruby-#{RUBY_VERSION}",
        mongoid_version: defined?(Mongoid) ? Mongoid::VERSION : nil
      }.to_json)
    end
  end

  describe '.endpoint' do
    it 'points to the v2 collector api endpoint' do
      expect(described_class.endpoint).to eq(URI("#{InformantCommon::Config.collector_host}/v2/client-info"))
    end
  end
end
