if defined?(ActiveModel)
  class TestModel
    include ActiveModel::Model
    include InformantCommon::ValidationTracking

    attr_accessor :name, :add_base_error

    validates :name, length: { minimum: 3 }
    validate :base_error

    def base_error
      errors.add(:base, 'This is a base error') if add_base_error
    end
  end
end
