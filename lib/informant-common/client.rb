module InformantCommon
  class Client
    SUPPORTED_REQUEST_METHODS = %w[POST PATCH PUT DELETE].freeze

    def self.enabled?
      @enabled
    end

    def self.disable!
      @enabled = false
    end

    def self.enable!
      @enabled = true
    end
    enable!

    def self.start_transaction!(request_method)
      if enabled? && SUPPORTED_REQUEST_METHODS.include?(request_method)
        Thread.current[:informant_transaction] = InformantCommon::Event::FormSubmission.new
      else
        reset_transaction!
      end
    end

    def self.current_transaction
      Thread.current[:informant_transaction] ||= InformantCommon::Event::MockFormSubmission.new
    end

    def self.reset_transaction!
      Thread.current[:informant_transaction] = nil
    end

    def self.process
      if current_transaction.valid?
        this_transaction = current_transaction
        transmit(this_transaction)
      end
    ensure
      reset_transaction!
    end

    def self.transmit(event)
      Thread.new(Client) do |client_class|
        Net::HTTP.start(*event.net_http_start_arguments) do |http|
          response = http.request(event.post_request)
          client_class.disable! if response.code == '401'
          response
        end
      end
    end
  end
end
