module InformantCommon
  class ParameterFilter
    def self.filter(name, value)
      Config.value_tracking? && !matcher.match(name) ? value : '[FILTERED]'
    end

    def self.matcher
      @matcher ||= Regexp.new(
        if Config.filter_parameters.any?
          /#{Config.filter_parameters.join('|')}/
        else
          /$^/
        end
      )
    end

    def self.reset!
      @matcher = nil
    end
  end
end
