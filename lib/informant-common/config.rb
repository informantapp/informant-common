module InformantCommon
  class Config
    def self.instance
      @instance ||= InformantCommon::Config.new
    end

    attr_writer :api_token, :exclude_models, :filter_parameters, :value_tracking

    def api_token
      @api_token ||= ENV.fetch('INFORMANT_API_KEY', nil)
    end

    def enabled?
      !api_token.nil? && api_token != ''
    end

    def exclude_models
      @exclude_models ||= []
    end

    def filter_parameters
      @filter_parameters ||= []
    end

    def value_tracking?
      if defined?(@value_tracking)
        @value_tracking
      else
        @value_tracking = true
      end
    end

    def self.collector_host
      @collector_host ||= ENV.fetch('INFORMANT_COLLECTOR_HOST', 'https://collector-api.informantapp.com')
    end

    def self.configure
      yield instance
    end

    def self.method_missing(name, *args)
      if instance.respond_to?(name)
        instance.public_send(name, *args)
      else
        super
      end
    end

    def self.respond_to_missing?(name)
      instance.respond_to?(name)
    end

    def self.reset!
      @instance = nil
    end
  end
end
