module InformantCommon
  module ValidationTracking
    if defined?(ActiveSupport)
      extend ActiveSupport::Concern

      included do
        set_callback(:validate, :after) do
          InformantCommon::Client.current_transaction.process_model(
            InformantCommon::Model::ActiveModel.new(self)
          )
        end
      end
    end
  end
end
