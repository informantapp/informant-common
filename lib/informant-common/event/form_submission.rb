module InformantCommon
  module Event
    class FormSubmission < InformantCommon::Event::Base
      attr_accessor :handler

      def process_model(model)
        return if model.nil? || Config.exclude_models.include?(model.name)

        models.reject! { |container| container.id == model.id }
        models << model
      end

      def models
        @models ||= []
      end

      def valid?
        models.any?
      end

      def as_json(*_args)
        {
          name: handler,
          models: models.map(&:as_json)
        }
      end

      def to_json(*_args)
        as_json.to_json
      end

      def self.endpoint
        @endpoint ||= URI("#{InformantCommon::Config.collector_host}/v2/form-submissions")
      end
    end
  end
end
