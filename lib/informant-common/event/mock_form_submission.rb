module InformantCommon
  module Event
    class MockFormSubmission < InformantCommon::Event::FormSubmission
      def process_model(_model); end

      def models
        []
      end
    end
  end
end
