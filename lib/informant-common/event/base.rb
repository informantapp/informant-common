module InformantCommon
  module Event
    class Base
      def self.endpoint
        raise 'Must implement'
      end

      def endpoint
        self.class.endpoint
      end

      def self.authorization_header_value
        @authorization_header_value ||= "Token token=\"#{InformantCommon::Config.api_token}\""
      end

      def authorization_header_value
        self.class.authorization_header_value
      end

      def post_request
        Net::HTTP::Post.new(endpoint,
                            'Authorization' => authorization_header_value,
                            'Content-Type' => 'application/json').tap { |r| r.body = to_json }
      end

      def self.net_http_start_arguments
        @net_http_start_arguments ||= [endpoint.host, endpoint.port, { use_ssl: endpoint.scheme == 'https' }]
      end

      def net_http_start_arguments
        self.class.net_http_start_arguments
      end
    end
  end
end
