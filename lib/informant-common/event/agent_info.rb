module InformantCommon
  module Event
    class AgentInfo < Base
      attr_accessor :agent_identifier, :framework_version

      def initialize(agent_identifier: nil, framework_version: nil)
        super()
        self.agent_identifier = agent_identifier
        self.framework_version = framework_version
      end

      def as_json(*_args)
        {
          agent_identifier: agent_identifier,
          framework_version: framework_version,
          runtime_version: "ruby-#{RUBY_VERSION}",
          mongoid_version: defined?(Mongoid) ? Mongoid::VERSION : nil
        }
      end

      def to_json(*_args)
        as_json.to_json
      end

      def self.endpoint
        @endpoint ||= URI("#{InformantCommon::Config.collector_host}/v2/client-info")
      end
    end
  end
end
