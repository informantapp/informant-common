module InformantCommon
  class FieldError
    attr_accessor :name, :message
    attr_reader :value

    def initialize(name, value, message = nil)
      self.name = name
      self.value = value
      self.message = message
    end

    def value=(value)
      @value = InformantCommon::ParameterFilter.filter(name, value)
    end

    def as_json(*_args)
      { name: name, value: value, message: message }
    end
  end
end
