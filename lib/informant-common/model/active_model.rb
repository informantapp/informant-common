module InformantCommon
  module Model
    class ActiveModel < Base
      def initialize(active_model)
        super(
          id: active_model.object_id,
          name: active_model.class.name,
          field_errors: []
        )

        active_model.errors.messages.each do |field, errors|
          add_error(field.to_s, extract_value(active_model, field), errors.first)
        end
      end

      private

      def extract_value(active_model, field)
        active_model.public_send(field)
      rescue StandardError
        nil
      end
    end
  end
end
