module InformantCommon
  module Model
    class Base
      attr_accessor :id, :name
      attr_writer :errors

      def initialize(id: nil, name: nil, field_errors: [])
        self.id = id
        self.name = name
        self.errors = field_errors
      end

      def errors
        @errors ||= []
      end

      def add_error(field_name, value, message)
        errors << FieldError.new(field_name, value, message)
      end

      def as_json(*_args)
        { name: name, errors: errors.map(&:as_json) }
      end

      def to_json(*_args)
        as_json.to_json
      end
    end
  end
end
