require 'net/http'

if defined?(Rake)
  require 'rake'
  Dir[File.join(File.dirname(__FILE__), 'tasks', '**/*.rake')].each { |rake| load rake }
end

module InformantCommon
  autoload :Client,             'informant-common/client'
  autoload :Config,             'informant-common/config'
  autoload :FieldError,         'informant-common/field_error'
  autoload :ParameterFilter,    'informant-common/parameter_filter'
  autoload :ValidationTracking, 'informant-common/validation_tracking'
  autoload :VERSION,            'informant-common/version'

  module Model
    autoload :ActiveModel, 'informant-common/model/active_model'
    autoload :Base,        'informant-common/model/base'
  end

  module Event
    autoload :Base,               'informant-common/event/base'
    autoload :AgentInfo,          'informant-common/event/agent_info'
    autoload :FormSubmission,     'informant-common/event/form_submission'
    autoload :MockFormSubmission, 'informant-common/event/mock_form_submission'
  end
end
